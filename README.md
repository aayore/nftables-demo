# nftables User-Based Restriction

[ [Homepage](https://www.nftables.org/projects/nftables/index.html) | [Wikipedia](https://en.wikipedia.org/wiki/Nftables) ]

#### What is nftables?

nftables is a newer (circa 2014) firewall replacement for iptables (1998).  I am not an expert.

#### What are user-based restrictions?

Inbound and outbound packets include a surprising amount of [metadata](https://wiki.nftables.org/wiki-nftables/index.php/Matching_packet_metainformation).  nftables can match this metadata to apply rules to more than just ports and addresses.

[User-based matching](https://wiki.nftables.org/wiki-nftables/index.php/Matching_packet_metainformation#Matching_by_socket_UID_.2F_GID) allows us to create rules on a per-user basis.

#### When/Why would you run it?

Most obviously, to mess with people.

Maybe less obvious...

Consider that most cloud-like environments have the ability to apply some type of permissions object: [AWS Instance Profile](https://docs.aws.amazon.com/IAM/latest/UserGuide/id_roles_use_switch-role-ec2.html), [GCP Service Account](https://cloud.google.com/compute/docs/access/service-accounts), Azure ... might have something, and OpenStack doesn't seem to have this.  But they all have an Instance MetaData Service (IMDS) - and most of them are accessible at the link-local address `169.254.169.254`.

Sensitive data may be stored in the instance metadata.  In the case of AWS (and maybe others - IDK), access to the IMDS allows use of the instance profile.  Which means that any user able to log in to the host can masquerade as the instance within your cloud environment.  Something like an automation host (think: Jenkins server or GitLab runner) may have extremely broad permissions.

> You probably don't want **joe_user** to run `aws ec2 delete-all-my-instances-and-everything-in-rds-and-s3-too`.  If Joe is supposed to be able to do this, he damn well better `sudo` it.

Food for thought:

- Would this be useful to, say, lock down `80` and `443` to the `apache` user?  I'm not sure it works like this with listeners.
- Can you think of other use cases?


#### How do you do it?

Make sure you're not doing this on a production machine.  Make sure there is no other firewall running before you start.

Also before we start, we're going to make sure we can talk to the IMDS:

```shell
curl http://169.254.169.254/latest/meta-data/
```

Install the `nftables` package with your operating system's package manager.  Could be `yum`, `apt`, `dnf`, etc.


See if there's a current ruleset:

```shell
nft list ruleset
```

This should be empty, so we're going to create a new table:

```shell
nft add table ip imds_lockdown_demo
```

Re-running `nft list ruleset` should show an empty table:

```
table ip imds_lockdown_demo {
}
```

Now we're going to create a chain

```shell
nft add chain ip imds_lockdown_demo DEMO_OUTPUT { type filter hook output priority filter; policy accept; }
```

Now running `nft list ruleset` should show our table with our empty chain:

```
table ip imds_lockdown_demo {
	chain DEMO_OUTPUT {
		type filter hook output priority filter; policy accept;
	}
}
```

Now we need to add our rule:

```shell
nft add rule ip imds_lockdown_demo DEMO_OUTPUT \
    ip daddr 169.254.169.254 meta skuid !=0 reject
```

Check `nft list ruleset` and you should see:

```
table ip imds_lockdown_demo {
	chain DEMO_OUTPUT {
		type filter hook output priority filter; policy accept;
		ip daddr 169.254.169.254 meta skuid != 0 reject
	}
}
```

Try running `curl http://169.254.169.254/latest/meta-data/` as both `root` and a non-root user.

That's it!

#### For Fun: Counters

Create a counter:

```shell
nft add counter imds_lockdown_demo imds_deny
```

Now the ruleset looks like this:

```
table ip imds_lockdown_demo {
	counter imds_deny {
		packets 0 bytes 0
	}

	chain DEMO_OUTPUT {
		type filter hook output priority filter; policy accept;
		ip daddr 169.254.169.254 meta skuid != 0 reject
	}
}
```

We can examine only the counter with this command:

```shell
nft list counter imds_lockdown_demo imds_deny
```

We have the counter, but we also need to tell nftables when to use it.  This means we have to replace our existing rule.  And to replace the rule, we need to know its "handle."

```shell
nft --handle list ruleset
```

Note the `# handle <number>` after certain (object) lines:

```
table ip imds_lockdown_demo { # handle 8
	counter imds_deny { # handle 3
		packets 0 bytes 0
	}

	chain DEMO_OUTPUT { # handle 1
		type filter hook output priority filter; policy accept;
		ip daddr 169.254.169.254 meta skuid != 0 reject # handle 2
	}
}
```

Now we can replace our rule:

```shell
nft replace rule ip imds_lockdown_demo DEMO_OUTPUT handle 2 \
    ip daddr 169.254.169.254 meta skuid != 0 counter name "imds_deny" reject
```

`nft list ruleset` will now show the `counter name "imds_deny"` statement in the rule:

```
table ip imds_lockdown_demo {
	counter imds_deny {
		packets 0 bytes 0
	}

	chain DEMO_OUTPUT {
		type filter hook output priority filter; policy accept;
		ip daddr 169.254.169.254 meta skuid != 0 counter name "imds_deny" reject
	}
}
```

Go try the `curl` command as a non-root user a few times.  You should now see non-zero values in the counter:

```
nft list counter imds_lockdown_demo imds_deny
table ip imds_lockdown_demo {
	counter imds_deny {
		packets 3 bytes 180
	}
}
```

You can also create a counter for `root`'s calls to the IMDS, you can do this with a second counter and a rule that doesn't `reject` the traffic.

```shell
nft add counter imds_lockdown_demo imds_allow

nft add rule ip imds_lockdown_demo DEMO_OUTPUT \
    ip daddr 169.254.169.254 meta skuid 0 counter name "imds_allow"
```

Final result:

```
table ip imds_lockdown_demo {
	counter imds_deny {
		packets 3 bytes 180
	}

	counter imds_allow {
		packets 20 bytes 1452
	}

	chain DEMO_OUTPUT {
		type filter hook output priority filter; policy accept;
		ip daddr 169.254.169.254 meta skuid != 0 counter name "imds_deny" reject
		ip daddr 169.254.169.254 meta skuid 0 counter name "imds_allow"
	}
}
```


Now that I made you go through all that command by command, you can actually save that block (the one immediately above - the output of `nft list ruleset`) as, say, `demo.nft` on your host and read the ruleset from the file...

```shell
nft -f demo.nft
```

#### Optimizing

We're going to accomplish a few goals here:

1. Usernames instead of UID's
1. Functional optimization
1. Logical optimization

##### Usernames

`nftables` runs in the kernel and needs to run quickly.  It doesn't want to do username lookups for every packet.  So the `nft` tool does username lookups when it loads the rule.  In the following example, we're going to combine the usernames with brackets to include multiple match targets in a single rule:

```
meta skuid { root, ec2-user } counter name "imds_allow" accept
```

##### Functional Optimization

About [chains](https://wiki.nftables.org/wiki-nftables/index.php/Configuring_chains):
- A **base chain** is registered into a hook (e.g. `output`) and will operate on all packets processed by that hook (e.g. all packets leaving the host).
- A **regular chain** is not registered to a hook, and will only operate on packets when specifically directed to do so.  (Side note: I have seen documentation that suggests you should be able to add a default `policy <policy> ;` to regular chains, but I have not been able to get this to work.  `man nft`: "Base chains also allow to set the chain’s policy..."  Which suggests that regular chains *cannot* have a default policy.)

Our ruleset runs the match against `169.254.169.254` two times.  If this grows, packets may be subjected to multiple match attempts even if we know they don't match.

We can optimize by switching to a different chain on the first match.  (Note that `jump` returns to the next statement after the jump, while `goto` does not return to the calling chain.)

We're going to consolidate the rules in our `DEMO_OUTPUT` (base) chain to:

```
ip daddr 169.254.169.254 goto IMDS_MGMT
```

We're going to create a new (regular) chain: `IMDS_MGMT`.  Because we know that packets in this chain will already have been subjected to the `ip dadr 169.254.169.254` match, we don't need to repeat it.

This means that a rule like this:

```
ip daddr 169.254.169.254 meta skuid 0 counter name "imds_allow"
```

Without the IP match becomes:

```
meta skuid 0 counter name "imds_allow"
```

Additionally, we don't want the `IMDS_MGMT` chain to use a hook.  It won't run by default; only when called.

##### Logical Optimization

Another advantage of separate chains is the isolation of logic into discrete units.  As your ruleset grows, you'll want to keep things organized for legibility/extensibility.

Putting these all together looks like this:

```
table ip imds_lockdown_demo {
        counter imds_deny {
                packets 0 bytes 0
        }

        counter imds_allow {
                packets 0 bytes 0
        }

        chain DEMO_OUTPUT {
                type filter hook output priority filter; policy accept;
                ip daddr 169.254.169.254 goto IMDS_MGMT
        }

        chain IMDS_MGMT {
                meta skuid { root, ec2-user } counter name "imds_allow" accept
                counter name "imds_deny" reject
        }
}
```